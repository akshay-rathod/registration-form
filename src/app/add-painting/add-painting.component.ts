import { PaintingService } from './../painting.service';
import { Component, OnInit } from '@angular/core';
import { ArtistService } from '../artist.service';

@Component({
  selector: "app-add-painting",
  templateUrl: "./add-painting.component.html",
  styleUrls: ["./add-painting.component.scss"]
})
export class AddPaintingComponent implements OnInit {
  artists: any = [];
  artist: any;
  painting: any;
  width: any;
  height: any;

  newPainting: any;

  constructor(private artistService: ArtistService, private paintingService: PaintingService) {
    this.painting = {
      name: '',
      artistId: '',
      description: '',
      picture: '',
      mrp: '',
      price: '',
      dimensions: '',
      format: ''
    };

    this.artist = {
      _id: '',
      name: ''
    };
  }

  ngOnInit() {
    this.artistService.getAllArtists().subscribe(artists => {
      this.artists = artists;
    });
  }

  createPainting() {
    this.painting.dimensions = `${this.width} x ${this.height}`;
    this.painting.artistId = this.artist;

    this.paintingService.createPainting(this.painting).subscribe(painting => {
      console.log(painting);
      this.newPainting = painting;
    });

    location.reload();
  }

  autoFormatPopulate() {
    if (this.width > this.height) this.painting.format = "Landscape";
    else if (this.width < this.height) this.painting.format = "Portrait";
    else this.painting.format = "Square";
  }
}
