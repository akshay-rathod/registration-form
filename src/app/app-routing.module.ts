import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddPaintingComponent } from './add-painting/add-painting.component';
import { CollectionComponent } from './collection/collection.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'add-painting', component: AddPaintingComponent },
  { path: 'collection', component: CollectionComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
