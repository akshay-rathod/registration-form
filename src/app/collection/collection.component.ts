import { ArtistService } from './../artist.service';
import { PaintingService } from './../painting.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss']
})
export class CollectionComponent implements OnInit {

  paintings: any;
  painting: any;

  artist: any;
  artists: any = [];

  width: any;
  height: any;

  constructor(private paintingService: PaintingService, private artistService: ArtistService) {
    this.artist = { _id: "", name: "" };
    this.painting = {
      name: '',
      artistId: '',
      description: '',
      picture: '',
      mrp: '',
      price: '',
      dimensions: '',
      format: ''
    };
   }

  ngOnInit() {
    this.paintingService.getAllPaintings().subscribe(paintings => {
      this.paintings = paintings;
    });

    this.artistService.getAllArtists().subscribe(artists => {
      this.artists = artists;
    });
  }

  getPicture(link: any) {
    console.log(link);
    this.painting = link;

    let x = this.painting.dimensions.split(' ');
    this.width = x[0]; this.height = x[2];
  }

  deletePainting(id) {
    this.paintingService.deletePainting(id).subscribe(painting => {
      console.log(painting);
      location.reload();
    });
  }

  updatePainting(id) {
    this.painting.dimensions = `${this.width} x ${this.height}`;
    this.painting.artistId = this.artist;

    this.paintingService.updatePainting(this.painting, id).subscribe(painting => {
      console.log(painting);
      // this.newPainting = painting;
    });

    location.reload();
  }

  autoFormatPopulate() {
    if (this.width > this.height) this.painting.format = "Landscape";
    else if (this.width < this.height) this.painting.format = "Portrait";
    else this.painting.format = "Square";
  }
}
