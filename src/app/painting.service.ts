import { environment } from './../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const API = environment.api;
const httpOptions = {
  headers: new HttpHeaders(
    {
      'Content-Type': 'application/json'
    }
  )
};

@Injectable({
  providedIn: "root"
})
export class PaintingService {
  constructor(private http: HttpClient) {}

  public createPainting(painting: any): any {
    return this.http.post(API + '/paintings/', painting, httpOptions);
  }

  public getAllPaintings(): any {
    return this.http.get(API + '/paintings/');
  }

  public deletePainting(id): any {
    return this.http.delete(API + '/paintings/' + id);
  }

  public updatePainting(painting: any, id: any): any {
    return this.http.put(API + '/paintings/' + id, painting, httpOptions);
  }
}
