import { ArtistService } from './../artist.service';
import { Component, OnInit, Directive } from '@angular/core';
import { PaintingService } from '../painting.service';

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  slide: any;
  paintings: any = [];

  constructor(
    private artistService: ArtistService,
    private paintingService: PaintingService
  ) {}

  ngOnInit() {
    this.paintingService.getAllPaintings().subscribe(paintings => {
      this.paintings = paintings;
    });
  }
}
