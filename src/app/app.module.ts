import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { AddPaintingComponent } from './add-painting/add-painting.component';
import { HomeComponent } from './home/home.component';
import { ArtistService } from './artist.service';

import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { PaintingService } from './painting.service';
import { SlideDirective } from './slide.directive';
import { CollectionComponent } from './collection/collection.component';

@NgModule({
  declarations: [
    AppComponent,
    AddPaintingComponent,
    HomeComponent,
    SlideDirective,
    CollectionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    SweetAlert2Module.forRoot()
  ],
  providers: [ ArtistService, PaintingService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
