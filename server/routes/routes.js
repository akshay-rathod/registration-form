`use strict`
const express = require('express')
const router = express.Router()

const paintings = require('../services/paintings-service')
const artists = require('../services/artist-service')

/* Paintings routes */
router.get('/paintings/', (req, res) => {
  paintings.getAllPaintings().then(data => {
    return res.status(200).json(data)
  })
})

router.get('/paintings/:id', (req, res) => {
  paintings.getPaintingById(req.params.id).then(data => {
    return res.status(200).json(data)
  })
})

router.post('/paintings', (req, res) => {
  /*
    Sample JSON Payload
    {
      "artistId": "5b12cff849912608a82d264c",
      "name": "Mona Lisa",
      "description": "\n                  Leonardo da Vinci's Mona Lisa is one of the most recognizable and famous works of art in the world, and also one of the most replicated and reinterpreted.\n                ",
      "picture": "https://i.imgur.com/8Yq2lyg.jpg",
      "mrp": 405.5,
      "price": 202.5,
      "dimensions": "18 x 27",
      "format": "Portrait"
    }
  */
  let data = req.body
  paintings.createPainting(data).then(newPainting => {
    res.status(200).json(newPainting)
  })
})

router.put('/paintings/:id', (req, res) => {
  /*
    Sample JSON Payload
    {
      "artistId": "5b12cff849912608a82d264c",
      "name": "Mona Lisa",
      "description": "\n                  Leonardo da Vinci's Mona Lisa is one of the most recognizable and famous works of art in the world, and also one of the most replicated and reinterpreted.\n                ",
      "picture": "https://i.imgur.com/8Yq2lyg.jpg",
      "mrp": 405.5,
      "price": 202.5,
      "dimensions": "18 x 27",
      "format": "Portrait"
    }
  */

  let data = req.body
  let paintingId = req.params.id

  paintings.updatePainting(paintingId, data).then(updatedPainting => {
    res.status(200).json(updatedPainting)
  })
})

router.delete('/paintings/:id', (req, res) => {
  paintings.deletePainting(req.params.id).then(deleted => {
    res.status(200).json(deleted)
  })
})

/* Artists Routes */
router.get('/artists/', (req, res) => {
  artists.getAllArtists().then(artists => {
    res.status(200).json(artists)
  })
})

module.exports = router
