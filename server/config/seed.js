`use strict`
const mongoose = require('mongoose')
const async = require('async')

const Painting = require('../models/Painting')
const Artist = require('../models/Artist')
const Counter = require('../models/Counter')

let init = async () => {
  let isDbPopulated = false

  await Counter.find({}, (err, counters) => {
    if (counters.length > 0) isDbPopulated = true
  })

  if (isDbPopulated) {
    console.log(`Db is already populated, skipping Seed.`)
  } else {

    console.log('Populating database now')

    let paintingsCounter = new Counter(
      {
        counterId: 'paintings-counter',
        seq: 0
      }
    )

    Counter.create(paintingsCounter).then(counter => {
      let daVinci = new Artist(
        {
          name: 'Leonardo Da Vinci'
        }
      )

      let michelangelo = new Artist(
        {
          name: 'Michelangelo'
        }
      )

      let giovanni = new Artist(
        {
          name: 'Giovanni Bellini'
        }
      )

      let van = new Artist(
        {
          name: 'Van Gogh'
        }
      )

      async.parallel(
        [
          (cb) => Artist.create(daVinci).then((artist) => cb(null, artist)),
          (cb) => Artist.create(michelangelo).then(( artist) => cb(null, artist)),
          (cb) => Artist.create(giovanni).then((artist) => cb(null, artist)),
          (cb) => Artist.create(van).then((artist) => cb(null, artist))
        ], (err) => {
          if (err) {
            console.log(`Error: ${err}`)
            return;
          } else {
            console.log('Artists Created!')

            let monaLisa = new Painting(
              {
                artistId: daVinci._id,
                name: `Mona Lisa`,
                description: `
                  Leonardo da Vinci's Mona Lisa is one of the most recognizable and famous works of art in the world, and also one of the most replicated and reinterpreted.
                `,
                picture: `https://i.imgur.com/8Yq2lyg.jpg`,
                mrp: 405.50,
                price: 202.50,
                dimensions: `18 x 27`,
                format: `Portrait`
              }
            )

            let salvator = new Painting(
              {
                artistId: daVinci._id,
                name: `Salvator Mundi`,
                description: `
                  Salvator Mundi is a painting of Christ as Salvator Mundi (Latin for "Savior of the World") by Italian artist Leonardo da Vinci, dated to c. 1500. The painting shows Jesus, in Renaissance dress, giving a benediction with his right hand raised and two fingers extended, while holding a transparent rock crystal orb in his left hand, signaling his role as savior of the world and master of the cosmos, and representing the 'crystalline sphere' of the heavens, as it was perceived during the Renaissance.
                `,
                picture: `https://i.imgur.com/6AIPZw1.jpg`,
                mrp: 405.50,
                price: 202.50,
                dimensions: `18 x 27`,
                format: `Portrait`
              }
            )

            let john = new Painting(
              {
                artistId: daVinci._id,
                name: `St. John The Baptist`,
                description: `
                  The piece depicts St. John the Baptist in isolation. Through the use of chiaroscuro, the figure appears to emerge from the shadowy background. St. John is dressed in pelts, has long curly hair and is smiling in an enigmatic manner reminiscent of Leonardo's famous Mona Lisa.
                `,
                picture: `https://i.imgur.com/Ij2WM16.jpg`,
                mrp: 378.00,
                price: 190.00,
                dimensions: `18 x 24`,
                format: `Portrait`
              }
            )

            let adam = new Painting(
              {
                artistId: michelangelo._id,
                name: 'The Creation of Adam',
                description: `
                  The Creation of Adam (Italian: Creazione di Adamo) is a fresco painting by Michelangelo, which forms part of the Sistine Chapel's ceiling, painted c. 1508–1512. It illustrates the Biblical creation narrative from the Book of Genesis in which God gives life to Adam, the first man.
                `,
                picture: `https://i.imgur.com/2QBFgBZ.jpg`,
                mrp: 450.00,
                price: 390.00,
                dimensions: `40 x 18`,
                format: `Landscape`

              }
            )

            let starryNight = new Painting(
              {
                artistId: van._id,
                name: 'The Starry Night',
                description: `
                  The Starry Night is an oil on canvas by the Dutch post - impressionist painter Vincent van Gogh
                `,
                picture: `https://i.imgur.com/WKCWS.jpg`,
                mrp: 430.20,
                price: 380.20,
                dimensions: `35 x 35`,
                format: 'Square'
              }
            )

            async.parallel(
              [
                (cb) => Painting.create(monaLisa).then((painting) => cb(null, painting)),
                (cb) => Painting.create(salvator).then((painting) => cb(null, painting)),
                (cb) => Painting.create(john).then((painting) => cb(null, painting)),
                (cb) => Painting.create(adam).then((painting) => cb(null, painting)),
                (cb) => Painting.create(starryNight).then((painting) => cb(null, painting))
              ], err => {
                if (err) {
                  console.log(`Error while creating Paintings: ${err}`)
                  return
                } else {
                  console.log(`Finished creating seed!`)
                }
            })
          }
        }
      )
    })
  }
}

init()
