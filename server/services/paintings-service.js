`use strict`
const Painting = require('../models/Painting')

module.exports = {
  getAllPaintings: () => {
    return Painting.find({}).populate('artistId').exec()
  },

  getPaintingById: (id) => {
    return Painting.findById(id).populate('artistId').exec()
  },

  createPainting: (newPainting) => {
    return Painting.create(newPainting).then(data => {
      return Painting.populate(data, 'artistId')
    })
  },

  updatePainting: (paintingId, updatedPainting) => {
    return Painting.findByIdAndUpdate(paintingId, updatedPainting, { new: true }).then(data => {
      return Painting.populate(data, 'artistId')
    })
  },

  deletePainting: (paintingId) => {
    return Painting.findByIdAndRemove(paintingId).exec()
  }
}
