`use strict`
const mongoose = require('mongoose')
const Artist = require('../models/Artist')

module.exports = {
  getAllArtists: () => {
    return Artist.find({}).exec()
  }
}
