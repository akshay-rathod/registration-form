`use strict`
global.__basedir = `${__dirname}/server `

const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const cors = require('cors')
const env = require('dotenv').config()

const routes = require('./routes/routes')


let app = express()
mongoose.Promise = global.Promise
console.log('Attempting connecting on: ' + `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB}`)
mongoose.connect(`mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB}`)
mongoose.connection.on(`error`, (err) => console.log(`Error while connecting to database: ${err}`))
mongoose.connection.once(`open`, () => {
  console.log(`Connection Established!`)
  const seed = require('./config/seed')
})

app.use(`*`, cors())
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.json({
  type: `application/vnd.api+json`
}))
app.use(bodyParser.urlencoded({ extended: true }))

app.listen(process.env.PORT, `0.0.0.0`, () => {
  console.log(`API server started on port ${process.env.PORT}`)
})

app.get('/', (req, res) => {
  return res.json(
    {
      message: 'API server is live!'
    }
  )
})

app.use(routes)

process.on(`SIGINT`, () => {
  mongoose.connection.close()
  process.exit(0)
})
