`use strict`
const mongoose = require('mongoose')
const Artist = require('../models/Artist')

let PaintingSchema = new mongoose.Schema(
  {
    artistId: {
      type: mongoose.Schema.ObjectId,
      ref: 'Artist'
    },
    name: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: false
    },
    picture: {
      type: String,
      required: false
    },
    mrp: {
      type: Number,
      required: true
    },
    price: {
      type: Number,
      required: true
    },
    dimensions: {
      type: String,
      required: true
    },
    format: {
      type: String,
      required: true
    }
  }
)

module.exports = mongoose.model('Painting', PaintingSchema)
