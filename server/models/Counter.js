`use strict`
const mongoose = require('mongoose')

let CounterSchema = new mongoose.Schema(
  {
    counterId: {
      type: String,
      required: true
    },
    seq: {
      type: Number,
      required: true
    }
  }
)

module.exports = mongoose.model('Counter', CounterSchema)
