`use strict`
const mongoose = require('mongoose')

let ArtistSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    }
  }
)

module.exports = mongoose.model('Artist', ArtistSchema)
